using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using ESRI.ArcGIS.DataSourcesGDB;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.GeoDatabaseExtensions;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Repository.Hierarchy;
using NDesk.Options;
using EsriSystem = ESRI.ArcGIS.esriSystem;


namespace SetSurrogateVersion
{
    internal class Program
    {
        private static Dictionary<string, object> ReadCommandLineArgs(bool executeParse = true)
        {
            var operation = string.Empty;
            var sdePath = string.Empty;
            var featureDatasetName = string.Empty;
            var fabricName = string.Empty;
            var versionName = string.Empty;
            var showHelp = false;


            var args = new Dictionary<string, object>();

            args["showhelp"] = false;

            var p = new OptionSet
            {
                {
                    "o=", "The operation to perform (e.g. SetSurrogateVersion, ClearSurrogateVersion).",
                    v => operation = v
                },
                {"sde=", "The path to the SDE File.", s => sdePath = s},
                {"fd=", "The name of the feature dataset.", s => featureDatasetName = s},
                {"fabric=", "The name of the parcel fabric.", s => fabricName = s},
                {"sv=", "The name of the surrogate version.", s => versionName = s},
                {"h|?|help|H", "Displays usage information.", delegate(string v) { showHelp = v != null; }}
            };


            try
            {
                if (executeParse)
                {
                    p.Parse(Environment.GetCommandLineArgs());
                    args["sde"] = sdePath;
                    args["fd"] = featureDatasetName;
                    args["fabric"] = fabricName;
                    args["sv"] = versionName;
                    args["showhelp"] = showHelp;
                    args["operation"] = operation;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                showHelp = true;
            }
            finally
            {
                args["OptionSet"] = p;
            }

            return args;
        }

        /// <summary>
        ///     Mains Method
        /// </summary>
        /// <param name="args">The args.</param>
        [STAThread]
        public static void Main(string[] args)
        {
            MethodDelegate dlgt = SendNotificationMessage;
            try
            {
                if (Environment.CommandLine.IndexOf("-nolog", StringComparison.InvariantCultureIgnoreCase) == -1)
                    XmlConfigurator.Configure();

                Log.Debug("After Configure");

                var parsedArgs = ReadCommandLineArgs();


                if ((bool)parsedArgs["showhelp"])
                {
                    ShowHelp(
                        (OptionSet)parsedArgs["OptionSet"]);
                    return;
                }

                var productCode = GetEsriProductCode();

                var envvars = GetEnvironmentVariables();

                dlgt.Invoke(string.Format("{0}: {1} started.", DateTime.Now, GetAssemblyName()),
                    string.Format("Command line:\n{0}\nCurrent directory:{1}\n\n\nEnvironment Variables\n{2}",
                        Environment.CommandLine, Environment.CurrentDirectory, envvars));

                //ESRI License Initializer generated code.
                if (!LicenseInitializer.InitializeApplication(new[] { productCode },
                    new EsriSystem.esriLicenseExtensionCode[] { }))
                {
                    Console.WriteLine(LicenseInitializer.LicenseMessage());
                    Console.WriteLine(
                        "This application could not initialize with the correct ArcGIS license and will shutdown.");
                    LicenseInitializer.ShutdownApplication();
                    //return;
                }
                else
                {
                    var operationArgs = parsedArgs["operation"].ToString().Trim();


                    var operations = new List<string>();

                    if (operationArgs.Contains(','))
                        operations.AddRange(operationArgs.Split(','));
                    else
                        operations.Add(operationArgs);

                    foreach (var operation in operations)
                    {
                        Log.DebugFormat("operation={0}", operation);


                        switch (operation)
                        {
                            case "SetSurrogateVersion":
                            case "Set":
                            case "set":
                            case "setsurrogatversion":


                                SetSurrogateVersion(
                                    parsedArgs,
                                    new Action<string, string>(dlgt));

                                break;

                            case "ClearSurrogateVersion":
                            case "Clear":
                            case "clearsurrogateversion":
                            case "clear":

                                ClearSurrogateVersion(
                                    parsedArgs,
                                    new Action<string, string>(dlgt));

                                break;

                            //case "QAPointAddresses":

                            //    QAPointAddresses(
                            //        parsedArgs,
                            //        new Action<string, string>(dlgt), stateName, zip4FileGeodatabasePath, fileGeodatabasePath, whereClause);

                            //    break;
                            //case "QAPointAddressSummary":

                            //    QAPointAddressSummary(
                            //        parsedArgs,
                            //        new Action<string, string>(dlgt));

                            //    break;
                            //case "QAGeocodingBSummary":

                            //    QAGeocodingBSummary(
                            //        parsedArgs,
                            //        new Action<string, string>(dlgt));

                            //    break;
                            default:
                                Log.Debug(string.Join(Environment.NewLine, args));


                                break;
                        }
                    }

                    OnShutdown();

                    //ESRI License Initializer generated code.
                    //Do not make any call to ArcObjects after ShutDownApplication()
                    LicenseInitializer.ShutdownApplication();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);

                SendNotificationMessage(string.Format("An error has occurred: {0}.", ex.Source),
                    string.Format("Command line:{0}\nError Message:{1}\n\nStack:{2}", Environment.CommandLine,
                        ex.Message,
                        ex.StackTrace));

                OnShutdown();

                LicenseInitializer.ShutdownApplication();
            }


            // string stateZip4ShapeFileName = 


            //string pathFile1 = @" C:\sandbox\Nokia\trunk\Census\TestData\CensusTestFile1.txt";
            //string pathFile2 = @" C:\sandbox\Nokia\trunk\Census\TestData\CensusTestFile1.txt";

            //string pathFile3 = @" C:\sandbox\Nokia\trunk\Census\TestData\USA_2013Q1_CensusID.txt";


            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            //var lc = LineCount(pathFile3);

            // // CreateHashFile(pathFile3, columnDelimiter);

            //stopwatch.Stop();

            //log.DebugFormat("CreateHashFile hashed {0} in {1}ms.", pathFile3, stopwatch.ElapsedMilliseconds);


            //List<LinkIDHash> file1Hashes = new List<LinkIDHash>();

            //List<LinkIDHash> file2Hashes = new List<LinkIDHash>();


            //file1Hashes = HashFileData(pathFile1, columnDelimiter);

            //file2Hashes = HashFileData(pathFile2, columnDelimiter);

            //List<string> orphanedLinkIDs = new List<string>();

            //foreach (var hash in file1Hashes)
            //{
            //    var f2hashetuples = from tuple in file2Hashes
            //                        where tuple.LinkID == hash.LinkID
            //                        select tuple;


            //    var hashmatches = from t in f2hashetuples
            //                      where RowHasher.Hasher.ByteArrayCompare(t.Hash, hash.Hash)
            //                      select t;

            //    if (hashmatches.Count() == 0)
            //    {

            //        orphanedLinkIDs.Add(hash.LinkID);
            //    }
            //}
        }

        /// <summary>
        ///     Sends the notification message.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <param name="message">The message.</param>
        private static void SendNotificationMessage(string subject, string message) { }

        private static string GetAssemblyName()
        {
            return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().Location);
        }

        /// <summary>
        ///     Called when [shutdown].
        /// </summary>
        private static void OnShutdown()
        {
            var logtext = GetLogText();

            var subject = string.Format("{0}: {1} Ending.", DateTime.Now, GetAssemblyName());

            SendNotificationMessage(subject,
                string.Format("Last 200 lines of the most recent log file: {0}{1}\n\n\n{2}",
                    "\n\r",
                    logtext,
                    string.Format("Command line:\n{0}", Environment.CommandLine)));
        }

        /// <summary>
        ///     Shows the help.
        /// </summary>
        /// <param name="p">The p.</param>
        private static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: SetSurrogateVersion [OPTIONS]+ message");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);


            Console.Write(Environment.NewLine + Environment.NewLine + Environment.NewLine);

            WriteHelpManual();
        }

        /// <summary>
        ///     Gets the environment variables.
        /// </summary>
        /// <returns></returns>
        private static string GetEnvironmentVariables()
        {
            var sb = new StringBuilder();

            foreach (var kvp in Environment.GetEnvironmentVariables())
            {
                var dictKvp = (DictionaryEntry)kvp;

                sb.Append(dictKvp.Key);
                sb.Append("=");
                sb.AppendLine(dictKvp.Value.ToString());
            }

            return sb.ToString();
        }

        /// <summary>
        ///     Gets the product code.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <exception cref="NotImplementedException"></exception>
        /// <returns></returns>
        private static EsriSystem.esriLicenseProductCode GetProductCode(string name)
        {
            switch (name.ToLower())
            {
                // 10.0 Enum
                //case "viewer":
                //    return EsriSystem.esriLicenseProductCode.esriLicenseProductCodeArcView;
                //case "editor":
                //    return EsriSystem.esriLicenseProductCode.esriLicenseProductCodeArcEditor;
                //case "professional":
                //    return EsriSystem.esriLicenseProductCode.esriLicenseProductCodeArcEditor;
                case "viewer":
                    return EsriSystem.esriLicenseProductCode.esriLicenseProductCodeBasic;
                case "editor":
                    return EsriSystem.esriLicenseProductCode.esriLicenseProductCodeStandard;
                case "professional":
                    return EsriSystem.esriLicenseProductCode.esriLicenseProductCodeAdvanced;

                default:
                    throw new NotImplementedException(string.Format("esriLicenseProductCode {0} no supported.", name));
            }
        }

        private static EsriSystem.esriLicenseProductCode GetEsriProductCode()
        {
            var esriSoftwareClass = "editor";

            var esriSoftwareClassEnvVar = Environment.GetEnvironmentVariable("Esri_SOFTWARE_CLASS");

            if (!string.IsNullOrEmpty(esriSoftwareClassEnvVar))
                esriSoftwareClass = esriSoftwareClassEnvVar;

            var productCode = GetProductCode(esriSoftwareClass);
            return productCode;
        }

        private static void WriteHelpManual()
        {
            var resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();

            foreach (var resourceName in resourceNames)
                Trace.WriteLine(resourceName);


            try
            {
                using (var stream = Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream("SetSurrogateVersion" + ".HelpManual.txt"))
                    if (stream != null)
                        using (var reader = new StreamReader(stream))
                        {
                            var result = reader.ReadToEnd();

                            if (!string.IsNullOrEmpty(result))
                                Console.WriteLine(result);
                        }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }
        }

        private static string GetLogText()
        {
            var text = string.Empty;

            try
            {
                var hierarchy = LogManager.GetRepository() as Hierarchy;
                if (hierarchy != null)
                {
                    var logger = hierarchy.Root; // WORKS FINE

                    if (logger != null)
                        foreach (var appender in logger.Appenders)
                            if (appender.Name.Equals("RollingLogFileAppender"))
                            {
                                var rollingFileAppender = appender as RollingFileAppender;
                                if (rollingFileAppender != null)
                                {
                                    var file = rollingFileAppender.File;

                                    var logtext = ReadEndTokens(file, 100, Encoding.Default, Environment.NewLine);

                                    text = logtext;
                                }
                            }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }


            return text;
        }

        private static string ReadEndTokens(string path, Int64 numberOfTokens, Encoding encoding, string tokenSeparator)
        {
            var sizeOfChar = encoding.GetByteCount("\n");
            var buffer = encoding.GetBytes(tokenSeparator);


            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                Int64 tokenCount = 0;
                var endPosition = fs.Length / sizeOfChar;

                for (Int64 position = sizeOfChar; position < endPosition; position += sizeOfChar)
                {
                    fs.Seek(-position, SeekOrigin.End);
                    fs.Read(buffer, 0, buffer.Length);

                    if (encoding.GetString(buffer) == tokenSeparator)
                    {
                        tokenCount++;
                        if (tokenCount == numberOfTokens)
                        {
                            var returnBuffer = new byte[fs.Length - fs.Position];
                            fs.Read(returnBuffer, 0, returnBuffer.Length);
                            return encoding.GetString(returnBuffer);
                        }
                    }
                }

                // handle case where number of tokens in file is less than numberOfTokens
                fs.Seek(0, SeekOrigin.Begin);
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                return encoding.GetString(buffer);
            }
        }

        private static bool SetSurrogateVersion(ref ICadastralFabric cadastralFabric, string surrogateName)
        {
            try
            {
                var datasetComponent = (IDatasetComponent)cadastralFabric;
                var dataElement = datasetComponent.DataElement;
                var cadastralFabric2 = (IDECadastralFabric2)dataElement;

                cadastralFabric2.SurrogateVersion = surrogateName;

                //Update the schema
                var schemaEdit = (ICadastralFabricSchemaEdit)cadastralFabric;
                schemaEdit.UpdateSchema((IDECadastralFabric)cadastralFabric2);


                return true;
            }
            catch (COMException comex)
            {
                Log.Error(comex.Message, comex);

                return false;
            }
        }

        #region Fields

        private delegate void MethodDelegate(string subject, string message);

        private static readonly LicenseInitializer LicenseInitializer = new LicenseInitializer();

        /// <summary>
        ///     log4net log
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Operation Methods

        private static void ClearSurrogateVersion(
            Dictionary<string, object> args,
            Action<string, string> notificationAction)
        {
            Log.Debug("Enter");

            if (notificationAction == null) throw new ArgumentNullException("notificationAction");
            
            var sdefile = args["sde"].ToString();
            var featureDataSetName = args["fd"].ToString();
            var fabricName = args["fabric"].ToString();
            var surrogateVersionName = string.Empty;

            var result = SetSurrogateVersion(
                sdefile,
                featureDataSetName,
                fabricName,
                surrogateVersionName);

            if (result)
                Log.InfoFormat(
                    "Parcel Fabric: '{0}' surrogate version cleared. ({1})",
                    fabricName,
                    sdefile
                    );
        }

        private static void SetSurrogateVersion(
            Dictionary<string, object> args,
            Action<string, string> notificationAction)
        {
            Log.Debug("Enter");

            if (notificationAction == null) throw new ArgumentNullException("notificationAction");

            var sdefile = args["sde"].ToString();
            var featureDataSetName = args["fd"].ToString();
            var fabricName = args["fabric"].ToString();
            var surrogateVersionName = args["sv"].ToString();

            // surrogateVersionName = string.Empty;

            var result = SetSurrogateVersion(
                sdefile,
                featureDataSetName,
                fabricName,
                surrogateVersionName);

            if (result)
                Log.InfoFormat(
                    "Parcel Fabric: '{0}' surrogate version set to: {1} ({2}).",
                    fabricName,
                    surrogateVersionName,
                    sdefile
                    );
        }

        /// <summary>
        ///     Sets the surrogate version.
        /// </summary>
        /// <param name="sdeFilePath">The sde file path.</param>
        /// <param name="featureDataSetName">Name of the feature data set.</param>
        /// <param name="fabricName">Name of the fabric.</param>
        /// <param name="surrogateVersionName">Name of the surrogate version.</param>
        /// <returns></returns>
        private static bool SetSurrogateVersion(string sdeFilePath, string featureDataSetName, string fabricName,
            string surrogateVersionName)
        {
            IWorkspaceFactory factory = new SdeWorkspaceFactoryClass();
            var workspace = factory.OpenFromFile(sdeFilePath, 0);

            var fw = workspace as IFeatureWorkspace;
            if (fw != null)
            {
                var fds = fw.OpenFeatureDataset(featureDataSetName);

                var extensionContainer = fds as IFeatureDatasetExtensionContainer;
                if (extensionContainer != null)
                {
                    var datasetExtension = extensionContainer.FindExtension(esriDatasetType.esriDTCadastralFabric);

                    var datasetContainer = datasetExtension as IDatasetContainer2;

                    if (datasetContainer != null)
                    {
                        var cadastralFabric =
                            // ReSharper disable once UseIndexedProperty
                            (ICadastralFabric)datasetContainer.get_DatasetByName(esriDatasetType.esriDTCadastralFabric, fabricName);

                        var b = SetSurrogateVersion(ref cadastralFabric, surrogateVersionName);


                        return b;
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("No Dataset Container for {0} in {1}", featureDataSetName, sdeFilePath));
                    }
                }
                else
                {
                    throw new ApplicationException(string.Format("No Dataset {0} in {1}", featureDataSetName, sdeFilePath));
                    
                }
            }
            else
            {
                throw new ApplicationException(string.Format("No Workspace For: {0}", sdeFilePath));
            }
        }

        #endregion
    }
}