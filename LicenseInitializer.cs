using System;
using ESRI.ArcGIS;

namespace SetSurrogateVersion
{
    internal partial class LicenseInitializer
    {
        public LicenseInitializer()
        {
            ResolveBindingEvent += BindingArcGISRuntime;
        }

        private void BindingArcGISRuntime(object sender, EventArgs e)
        {
            if (!RuntimeManager.Bind(ProductCode.Desktop))
            {
                // Failed to bind, announce and force exit
                Console.WriteLine("Invalid ArcGIS runtime binding. Application will shut down.");
                Environment.Exit(0);
            }
        }
    }
}